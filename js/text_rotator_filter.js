/**
 * @file
 * Description.
 */

(function ($, Drupal,  once, drupalSettings) {

  Drupal.behaviors.text_rotator_filter = {
    attach: function (context, settings) {
      $(once('text_rotator_filter', '.filter-rotate')).textrotator({
        animation: drupalSettings.text_rotator_filter.animation,
        separator: "|",
        speed: drupalSettings.text_rotator_filter.speed
      });
    }
  }
})(jQuery, Drupal, once, drupalSettings);
